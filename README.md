## initialize the LibraSDK for CoAP

    coapManager = CoapManager.getInstance(this,"d7f619b81a3d37697977b00dd7d4e7f17d0aaa37");
    coapManager.initCoapServer("192.168.1.7","/light");
    coapManager.SendRequest(CoapManager.GET);

## Add CoAPListener
    public class CoapClientActivity extends AppCompatActivity implements CoAPListener {
        .....
    }
    
## implement interface for LibraSDK
    @Override
        public void onReceiveCoAP(String type, String uri, String payload) {
            Log.e("JH","OnReceiveCoAP " + type + " " + uri+ " "+payload);
            ....
        }

    @Override
    public void onFailedToReceiveCoAP(int errorCode, String errorMsg) {
        Log.e("JH","onFailedToReceiveCoAP " + errorCode + " " + errorMsg );
        ....
    }
