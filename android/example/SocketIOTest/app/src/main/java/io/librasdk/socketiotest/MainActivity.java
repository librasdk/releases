package io.librasdk.socketiotest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.net.MalformedURLException;
import io.socket.client.Socket;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private TextView tvMain;
    private EditText etMsg;
    private Button btn_send,btn_bye;
    SocketIOClient mSocketClient;
    private static final String serverUrl = "http://192.168.1.2:8001";
    private static final String apiKey = "111111111";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvMain = findViewById(R.id.tvMain);
        btn_send = findViewById(R.id.btn_send);
        btn_bye = findViewById(R.id.btn_bye);

        try {
            mSocketClient = SocketIOClient.getInstance(apiKey);
            mSocketClient.connect(serverUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("JH","Send....");
                SocketIOClient.emit("notice", "3333333");
            }
        });

        btn_bye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("JH","Bye....");
                SocketIOClient.emit("bye", "");
               // SocketIOClient.getSocket().emit("bye");
               // SocketIOClient.getSocket().disconnect();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SocketIOClient.emit("bye", "");
        SocketIOClient.getSocket().disconnect();
    }

}