package io.librasdk.socketiotest;
import android.app.Activity;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

public class SocketIOClient {
    //private static final String serverUrl = "http://192.168.1.2:8001";
    private static Socket socket;
  ////////  private static SocketIOClient instance;
    private static Activity act;
    private static String id;

    public SocketIOClient(String apiKey) {
        this.id = apiKey;
    }


    // Singleton
    private static SocketIOClient instance = null;
    public static SocketIOClient getInstance(String apiKey) {
        if (instance == null) {
            instance = new SocketIOClient(apiKey);
            instance.initID(apiKey);
        }
        return instance;
    }

    public boolean connect(String serverUrl) throws MalformedURLException{

        if (SocketIOClient.getSocket() == null) {
            try {
                SocketIOClient.setSocket(IO.socket(serverUrl));
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return false;
            }
            catch(Exception e) { }
            SocketIOClient.connectIO();
        }
        return true;
    }
/*
    public static void initInstance(String uid) throws MalformedURLException {
        if (instance == null) {
          //  instance = new SocketIOClient();
           // instance.initID(uid);
            if (SocketIOClient.getSocket() == null) {

                try {
                    SocketIOClient.setSocket(IO.socket(serverUrl));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
            SocketIOClient.connectIO();
        }
    }
*/
    public static void setActivity(Activity a) {
        SocketIOClient.act = a;
    }

    public static Socket getSocket() {
        return socket;
    }

    public static void setSocket(Socket socket) {
        SocketIOClient.socket = socket;
    }

    public String getId() {
        return id;
    }

    private void initID(String uid) {
        if (SocketIOClient.id == null) {
            SocketIOClient.id = uid;
        }
    }

    public static void connectIO() throws MalformedURLException {
        try {
            SocketIOClient.getSocket().connect();
            SocketIOClient.getSocket().on("reply", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("JH", "reply: receive message...");
                    String res = (String) args[0];
                    Log.d("JH", "Response:" + res);

                }
            });
        }catch(Exception e) { }

    }

    public static void disconnect() {
        try {
            if (SocketIOClient.getSocket().connected() == true) {
                Log.d("JH", "disconnect...ok");
                SocketIOClient.getSocket().disconnect();
            } else {
                Log.d("JH", "disconnect...ng");
            }
        }
        catch(Exception e) { }

    }

    public static void emit(String event, Object args) {
            if (SocketIOClient.getSocket().connected() == false) {
                SocketIOClient.getSocket().connect();
                Log.d("JH", "it is not connected...try to reconnect...");
            }
            SocketIOClient.getSocket().emit(event, args);

    }

    public static void emitWithAcknowledge(String event, Object args) {
            if (SocketIOClient.getSocket().connected() == false) {
                SocketIOClient.getSocket().connect();
                Log.d("JH", "it is not connected...try to reconnect...");
            }
            SocketIOClient.getSocket().emit(event, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("JH", "emit 2222 ");
                }
            }, args);

    }

}
